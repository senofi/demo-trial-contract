package io.consortia.trial.contract;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TrialCaseDTO {
    
    private String status;//new, in process, closed, accepted
    private String caseId;
    private String description;
    private String subjectName;
    private String siteName;
    private Date submissionDate;
//
//    private Date closedDate;
//    private String closedBy;
//    private String acceptedBy;
//    private Date acceptedDate;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCaseId() {
        return caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public Date getSubmissionDate() {
        return submissionDate;
    }

    public void setSubmissionDate(Date submissionDate) {
        this.submissionDate = submissionDate;
    }
 

    @JsonCreator
    public static TrialCaseDTO Create(String jsonString) {
        ObjectMapper mapper = new ObjectMapper();
        TrialCaseDTO caseDto = null;
        try {
            caseDto = mapper.readValue(jsonString, TrialCaseDTO.class);
        } catch (Exception e) {
            return new TrialCaseDTO();
        }
        return caseDto;
    }

    public String toJson() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
 
}
